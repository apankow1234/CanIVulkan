# My Vulkan Abilities

## Can I Vulkan?

Figure if your machine is capable of running Vulkan
```bash
bash ./CanIVulkan.sh
```

Literally returns `yes` or `no`

## What Can Vulkan?
Figure out which hardware in your machine is capable of running Vulkan
```bash
bash ./WhatCanVulkan.sh
```

Returns list of your devices which are known to run Vulkan
* `DISCRETE : GPU_TITLE`
* `INTEGRATED : GPU_TITLE`

# Examples
![Multiple Vulkan Devices - Ubuntu 16.04](images/multiple-ubuntu_16.png "Multiple Vulkan Devices - Ubuntu 16.04")
![Multiple Vulkan Devices - Ubuntu 14.04](images/solo-ubuntu_14.png "One Vulkan Device - Ubuntu 14.04")
