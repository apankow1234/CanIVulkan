#!/bin/bash
#==================================================================#
# Can I Vulkan?
# By: Andrew Pankow
# 
# Check hardware and compare against database
#------------------------------------------------------------------#
declare -a successful_devices=()
for check in $(lspci -nn | grep -i "display\|vga\|3d\|2d" | grep -Poi ".*\[.*\:\K.*(?=\])"); do
	IFS='' finds=$( wget -q "http://vulkan.gpuinfo.org/api/v2/getjsonreportlist" -O - | grep -Pi "0x$check" )
	IFS=$'\n' successful_devices+=("$finds")
done
if [ ${#successful_devices[*]} -gt 0 ]; then echo yes; else echo no; fi
