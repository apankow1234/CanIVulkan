#!/bin/bash
#==================================================================#
# What Can Vulkan?
# By: Andrew Pankow
# 
# Check hardware and compare against database
#------------------------------------------------------------------#
db_src="http://vulkan.gpuinfo.org/api/v2/getjsonreportlist"
declare -a successful_devices=()

local_devices=$(lspci -nn \
	| grep -i "display\|vga\|3d\|2d" \
	| grep -Poi ".*\[.*\:\K.*(?=\])")
for check in $local_devices; do
	device_record=GetDeviceInfo
	IFS=$'\n' command eval 'device_record=($( wget -q "$db_src" -O - \
			| grep -Pi "0x$check" -B 1 -A 12 \
			| head -14 \
			| sed "s/\"//g;s/\,//g" \
			| sort -u ))'
	gpu_name=`echo ${device_record[2]} | awk -F[':'] '{print $2}' `
	gpu_type=`echo ${device_record[3]} | awk -F[':|_'] '{print $2}' `
	IFS=$'\n' successful_devices+=("$gpu_type : $gpu_name")
done
echo "${successful_devices[*]}" | sort -u
